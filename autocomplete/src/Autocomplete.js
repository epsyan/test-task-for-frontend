import spinnerSVG from './spinner.svg';
import './styles/Autocomplete.less';

export const CSS_CLASS = {
	CONTAINER: 'autocomplete-container',
	INPUT: 'autocomplete-input',
	RESULTS_CONTAINER: 'autocomplete-results',
	RESULT_ROW: 'autocomplete-result-row',
	SPINNER: 'autocomplete-spinner'
};
Object.freeze(CSS_CLASS);

const defaultConfig = {
	searchMinChars: 2,
	useSpinner: false,
	placeholder: 'Search...'
};

export default class Autocomplete {
	constructor(targetElem, config = {}) {
		this.config = {
			...defaultConfig,
			...config
		};

		this.checkConfig(targetElem, this.config);
		this.targetElem = targetElem;
		this.isDOMBuilt = false;
		this.keyboardPosition = -1;

		this.buildDOM();
		this.applyCustomClasses();
	}

	buildDOM() {
		if (this.isDOMBuilt) return;

		this.containerElem = document.createElement('div');
		this.containerElem.className = CSS_CLASS.CONTAINER;

		this.inputElem = document.createElement('input');
		this.inputElem.className = CSS_CLASS.INPUT;
		this.inputElem.placeholder = this.config.placeholder;
		this.inputElem.addEventListener('keyup', this.handleInput.bind(this));
		this.inputElem.addEventListener('keyup', this.handleKeyboardNavigation.bind(this));

		this.resultsContainerElem = document.createElement('ul');
		this.resultsContainerElem.className = CSS_CLASS.RESULTS_CONTAINER;

		this.targetElem.appendChild(this.containerElem);
		this.containerElem.appendChild(this.inputElem);

		if (this.config.useSpinner) {
			this.spinnerElem = document.createElement('img');
			this.spinnerElem.src = spinnerSVG;
			this.spinnerElem.className = CSS_CLASS.SPINNER;
			this.addClass(this.spinnerElem, 'hidden');
			this.containerElem.appendChild(this.spinnerElem);
		}

		this.containerElem.appendChild(this.resultsContainerElem);

		this.isDOMBuilt = true;
	}

	handleInput() {
		const searchString = this.inputElem.value;

		clearTimeout(this.inputTimeout);

		if (searchString.length === 0) {
			this.clearResultsDOM();
			this.prevSearchString = searchString;
		}

		if (!this.shouldDoSearch(searchString)) {
			this.hideSpinner();
			return;
		}

		this.showSpinner();
		this.inputTimeout = setTimeout(() => {
			this.prevSearchString = searchString;
			this.doSearch(searchString);
			this.hideSpinner();
		}, this.config.searchTimeout);
	}

	shouldDoSearch(searchString) {
		return (searchString.length >= this.config.searchMinChars && searchString !== this.prevSearchString);
	}

	doSearch(searchString = '') {
		const dataForSearch = this.getDataForSearch(searchString);

		this.results = (
			dataForSearch
				.map((entry) => entry[this.dataSet.sourceSearchField])
				.filter((entry) => entry.toLowerCase().includes(searchString.trim().toLowerCase()))
		);

		this.showResults();
	}

	getDataForSearch(searchString) {
		for (let dataSet of this.config.data) {
			const source = dataSet.source(searchString);

			if (source && Array.isArray(source) && source.length) {
				this.dataSet = dataSet;
				return source;
			}
		}
	}

	showResults() {
		this.clearResultsDOM();
		if (!this.results || !this.results.length) return;

		this.results.forEach(this.showResultRow.bind(this));
	}

	showResultRow(result) {
		if (!result) return;

		const { onRender } = this.dataSet;
		const customResultRowClass = this.config.customClasses && this.config.customClasses.resultRow;

		const resultRowElem = document.createElement('li');
		resultRowElem.className = CSS_CLASS.RESULT_ROW;
		resultRowElem.innerText = onRender ? onRender(result) : result;
		resultRowElem.addEventListener('click', this.handleResultSelect.bind(this, result));
		this.addClass(resultRowElem, customResultRowClass);

		this.resultsContainerElem.appendChild(resultRowElem);
	}

	handleResultSelect(result) {
		if (!result) return;

		this.inputElem.value = result;
		this.prevSearchString = result;
		this.clearResultsDOM();

		if (this.config.onResultSelected) {
			this.config.onResultSelected(result);
		}
	}

	clearResultsDOM() {
		this.resultsContainerElem.innerHTML = '';
	}

	handleKeyboardNavigation(e) {
		switch(e.code) {
			case 'ArrowDown':
				e.preventDefault();

				if (this.keyboardPosition < this.results.length - 1) {
					this.keyboardPosition += 1;
				}
				break;

			case 'ArrowUp':
				e.preventDefault();

				if (this.keyboardPosition > 0) {
					this.keyboardPosition -= 1;
				}
				break;

			case 'Enter':
				e.preventDefault();

				if (this.keyboardPosition > -1) {
					this.handleResultSelect(this.results[this.keyboardPosition]);
					this.resetKeyboardPosition();
				}
				return;
		}

		this.highlightResult(this.keyboardPosition);
	}

	resetKeyboardPosition() {
		this.keyboardPosition = -1;
	}

	highlightResult(position) {
		const children = Array.from(this.resultsContainerElem.children);

		children.forEach((el) => this.removeClass(el, 'active'));
		this.addClass(children[position], 'active');
	}

	showSpinner() {
		if(!this.config.useSpinner) return;

		this.removeClass(this.spinnerElem, 'hidden');
	}

	hideSpinner() {
		if(!this.config.useSpinner) return;

		this.addClass(this.spinnerElem, 'hidden');
	}

	applyCustomClasses() {
		if (!this.config.customClasses) return;

		const { container = '', input = '', resultsContainer = '' } = this.config.customClasses;

		this.addClass(this.containerElem, container);
		this.addClass(this.inputElem, input);
		this.addClass(this.resultsContainerElem, resultsContainer);
	}

	addClass(elem, cssClass) {
		if (!elem || !cssClass) return;

		elem.classList.add(cssClass);
	}

	removeClass(elem, cssClass) {
		if (!elem || !cssClass) return;

		elem.classList.remove(cssClass);
	}

	checkConfig(targetElem, config) {
		const notUndefined = (v) => typeof v !== 'undefined';

		if (!targetElem || typeof targetElem !== 'object' || !targetElem.nodeType) {
			throw new Error('You should specify DOM element as target');
		}

		if (!config.data || !Array.isArray(config.data) || !config.data.length) {
			throw new Error('You should specify data array in format [{ source: fn, sourceSearchField: string, onRender: fn }, ...] ');
		} else {
			config.data.forEach((d, i) => {
				if (!d.source || typeof d.source !== 'function') {
					throw new Error(`data[${i}].source should be function`);
				}

				if (!d.sourceSearchField || typeof d.sourceSearchField !== 'string') {
					throw new Error(`data[${i}].sourceSearchField should be string`);
				}

				if (notUndefined(d.onRender) && typeof d.onRender !== 'function') {
					throw new Error(`data[${i}].onRender should be function`);
				}
			})
		}

		if (notUndefined(config.searchTimeout) && (typeof config.searchTimeout !== 'number' || config.searchTimeout <= 0)) {
			throw new Error('searchTimeout param should be a number greater than zero');
		}

		if (notUndefined(config.useSpinner) && typeof config.useSpinner !== 'boolean') {
			throw new Error('useSpinner param should be a boolean value');
		}

		if (notUndefined(config.onResultSelected) && typeof config.onResultSelected !== 'function') {
			throw new Error('onResultSelected param should be a function');
		}
	}
}
