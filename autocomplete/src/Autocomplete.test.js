import Autocomplete, { CSS_CLASS } from './Autocomplete';
jest.mock('./styles/Autocomplete.less', () => jest.fn());
jest.mock('./spinner.svg', () => jest.fn());

describe('Autocomplete lib tests', () => {
	let requiredConfig = {
		data: [{
			source: () => [{ city: 'Kyiv' }, { city: 'Big-Kyiv' }, { city: 'Kharkiv' }],
			sourceSearchField: 'city'
		}],
		searchTimeout: 100
	};

	let targetContainer;

	const createInstance = (config = {}) => new Autocomplete(targetContainer, { ...requiredConfig, ...config });

	const getByClass = (cssClass) => targetContainer.getElementsByClassName(cssClass);

	beforeEach(() => {
		document.body.innerHTML = '';

		targetContainer = document.createElement('div');
		targetContainer.id = 'autocomplete-target';

		document.body.append(targetContainer);
	});

	describe('throw if config not ok', function () {
		it('should throw if target not an element', function () {
			expect(() => new Autocomplete('ImAnElement', requiredConfig)).toThrow();
		});

		it('should throw if data not in right format', function () {
			expect(() => createInstance({ data: 'abc' })).toThrow();
			expect(() => createInstance({ data: [] })).toThrow();
			expect(() => createInstance({ data: [{ source: 'abc' }] })).toThrow();
			expect(() => createInstance({ data: [{ source: () => [{ f: 'abc' }] }] })).toThrow();
			expect(() => createInstance({ data: [{ source: () => [{ f: 'abc' }], sourceSearchField: null }] })).toThrow();
			expect(() => createInstance({ data: [{ source: () => [{ f: 'abc' }], sourceSearchField: 'f', onRender: false }] })).toThrow();
		});

		it('should throw if timeout not positive number', function () {
			expect(() => createInstance({ searchTimeout: 'one hundred' })).toThrow();
			expect(() => createInstance({ searchTimeout: -100 })).toThrow();
		});

		it('should throw if spinner not a boolean', function () {
			expect(() => createInstance({ useSpinner: 'NO' })).toThrow();
		});

		it('should throw if onResultSelected is not a func', function () {
			expect(() => createInstance({ onResultSelected: 'do nothing'})).toThrow();
		});
	});

	it('should assign right target node', function () {
		const acInstance = createInstance();
		expect(acInstance.targetElem).toBe(targetContainer);
	});

	it('should build right DOM', function () {
		createInstance();

		expect(getByClass(CSS_CLASS.CONTAINER).length).toBe(1);
		expect(getByClass(CSS_CLASS.INPUT).length).toBe(1);
		expect(getByClass(CSS_CLASS.RESULTS_CONTAINER).length).toBe(1);
		expect(getByClass(CSS_CLASS.SPINNER).length).toBe(0);
	});

	it('should assign custom classes', function () {
		const container = 'custom-container';
		const input = 'custom-input';
		const resultsContainer = 'custom-results-container';

		const config = { customClasses: { container, input, resultsContainer }};

		createInstance(config);

		expect(getByClass(CSS_CLASS.CONTAINER)[0].classList.contains(container)).toBe(true);
		expect(getByClass(CSS_CLASS.INPUT)[0].classList.contains(input)).toBe(true);
		expect(getByClass(CSS_CLASS.RESULTS_CONTAINER)[0].classList.contains(resultsContainer)).toBe(true);
	});

	it('should create spinner if configured', function () {
		createInstance({ useSpinner: true });

		expect(getByClass(CSS_CLASS.SPINNER).length).toBe(1);
	});

	it('should show and hide spinner', function () {
		jest.useFakeTimers();

		const ac = createInstance({ useSpinner: true });

		expect(getByClass(CSS_CLASS.SPINNER)[0].classList.contains('hidden')).toBe(true);

		ac.inputElem.value = 'Kyiv';
		ac.handleInput();

		expect(getByClass(CSS_CLASS.SPINNER)[0].classList.contains('hidden')).toBe(false);

		jest.runOnlyPendingTimers();

		expect(getByClass(CSS_CLASS.SPINNER)[0].classList.contains('hidden')).toBe(true);
	});

	it('should perform search and show results', function () {
		jest.useFakeTimers();
		const ac = createInstance();

		ac.inputElem.value = 'Kyiv';
		ac.handleInput();
		jest.runOnlyPendingTimers();

		expect(getByClass(CSS_CLASS.RESULT_ROW).length).toBe(2);
		expect(getByClass(CSS_CLASS.RESULT_ROW)[0].innerText).toBe('Kyiv');
		expect(getByClass(CSS_CLASS.RESULT_ROW)[1].innerText).toBe('Big-Kyiv');
	});

	it('should perform search starting from minChars', function () {
		jest.useFakeTimers();

		const ac = createInstance({ searchMinChars: 3 });

		jest.spyOn(ac, 'doSearch');

		ac.inputElem.value = '';
		ac.handleInput();
		jest.runOnlyPendingTimers();
		expect(ac.doSearch).not.toBeCalled();

		ac.inputElem.value = 'Ky';
		ac.handleInput();
		jest.runOnlyPendingTimers();
		expect(ac.doSearch).not.toBeCalled();

		ac.inputElem.value = 'Kyiv';
		ac.handleInput();
		jest.runOnlyPendingTimers();
		expect(ac.doSearch).toBeCalledWith('Kyiv');
	});

	it('should handle result select', function () {
		const onResultSelected = jest.fn();
		const ac = createInstance({ onResultSelected });
		jest.spyOn(ac, 'clearResultsDOM');

		ac.dataSet = {};
		ac.results = ['Kyiv', 'Big-Kyiv'];
		ac.showResults();
		ac.handleResultSelect('Big-Kyiv');

		expect(getByClass(CSS_CLASS.INPUT)[0].value).toBe('Big-Kyiv');
		expect(ac.clearResultsDOM).toBeCalled();
		expect(onResultSelected).toBeCalledWith('Big-Kyiv');
	});

	describe('data sets and callbacks', function () {
		let source1 = {
			source: (searchString) => searchString === 'test' ? [{ key: 'search test' }] : [],
			sourceSearchField: 'key'
		};
		let source2 = {
			source: () => [{ city: 'Kyiv' }, { city: 'Big-Kyiv' }, { city: 'Kharkiv' }],
			sourceSearchField: 'city'
		};

		it('should iterate through sources until not empty array returned', function () {
			const ac = createInstance({ data: [source1, source2] });

			jest.spyOn(ac, 'getDataForSearch');

			ac.getDataForSearch('Kyiv');

			expect(ac.getDataForSearch).toReturnWith([{ city: 'Kyiv' }, { city: 'Big-Kyiv' }, { city: 'Kharkiv' }]);
			expect(ac.dataSet).toBe(source2);

			ac.getDataForSearch('test');

			expect(ac.getDataForSearch).toReturnWith([{ key: 'search test' }]);
			expect(ac.dataSet).toBe(source1);
		});

		it('should use render callback from not empty dataSet', function () {
			const source1WCb = {...source1, onRender: (result) => `${result} - choose me` };
			const source2WCb = {...source2, onRender: (result) => `${result} - @test` };

			const ac = createInstance({ data: [source1WCb, source2WCb] });

			ac.doSearch('Kyiv');

			expect(getByClass(CSS_CLASS.RESULT_ROW)[0].innerText).toBe('Kyiv - @test');
			expect(getByClass(CSS_CLASS.RESULT_ROW)[1].innerText).toBe('Big-Kyiv - @test');

			ac.doSearch('test');

			expect(getByClass(CSS_CLASS.RESULT_ROW)[0].innerText).toBe('search test - choose me');
		});
	});

	describe('keyboard events', function () {
		let ac;

		const performKeyboardEvent = (code, times = 1) => {
			while(times > 0) {
				ac.handleKeyboardNavigation({ code, preventDefault: jest.fn() });
				times--;
			}
		};

		beforeEach(() => {
			ac = createInstance();
			ac.dataSet = {};
			ac.results = ['Kyiv', 'Big-Kyiv'];
			ac.showResults();
		});

		it('should handle keyboard arrow down right', function () {
			performKeyboardEvent('ArrowDown');

			expect(getByClass(CSS_CLASS.RESULT_ROW)[0].classList.contains('active')).toBe(true);
			expect(getByClass(CSS_CLASS.RESULT_ROW)[1].classList.contains('active')).toBe(false);

			performKeyboardEvent('ArrowDown', 5);

			expect(getByClass(CSS_CLASS.RESULT_ROW)[0].classList.contains('active')).toBe(false);
			expect(getByClass(CSS_CLASS.RESULT_ROW)[1].classList.contains('active')).toBe(true);
		});

		it('should handle keyboard arrow up', function () {
			ac.keyboardPosition = 1;
			ac.highlightResult(ac.keyboardPosition);

			expect(getByClass(CSS_CLASS.RESULT_ROW)[0].classList.contains('active')).toBe(false);
			expect(getByClass(CSS_CLASS.RESULT_ROW)[1].classList.contains('active')).toBe(true);

			performKeyboardEvent('ArrowUp');

			expect(getByClass(CSS_CLASS.RESULT_ROW)[0].classList.contains('active')).toBe(true);
			expect(getByClass(CSS_CLASS.RESULT_ROW)[1].classList.contains('active')).toBe(false);

			performKeyboardEvent('ArrowUp', 5);

			expect(getByClass(CSS_CLASS.RESULT_ROW)[0].classList.contains('active')).toBe(true);
			expect(getByClass(CSS_CLASS.RESULT_ROW)[1].classList.contains('active')).toBe(false);
		});

		it('should handle keyboard enter', function () {
			jest.spyOn(ac, 'handleResultSelect');
			ac.keyboardPosition = 1;
			ac.highlightResult(ac.keyboardPosition);

			performKeyboardEvent('Enter');

			expect(ac.handleResultSelect).toBeCalledWith('Big-Kyiv');
		});

	});
});