const path = require('path');

const TerserPlugin = require('terser-webpack-plugin');
const LessPluginCleanCSS = require('less-plugin-clean-css');
const LessPluginAutoprefix = require('less-plugin-autoprefix');

module.exports = {
	entry: path.resolve(__dirname, 'src/index.js'),
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.js'
	},
	optimization: {
		minimizer: [new TerserPlugin()]
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: 'babel-loader'
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader']
			},
			{
				test: /\.less$/,
				use: [
					{
						loader: 'style-loader',
					},
					{
						loader: 'css-loader',
						options: {
							sourceMap: true
						}
					},
					{
						loader: 'less-loader',
						options: {
							plugins: [
								new LessPluginCleanCSS(),
								new LessPluginAutoprefix({ browsers: ['last 2 versions'] })
							]
						}
					}
				]
			},
			{
				test: /\.svg(\?.*)?$/,
				use: [
					'url-loader',
					'svg-transform-loader'
				]
			},
		]
	},
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
	}
};
