import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import '../../../autocomplete/src';

const Autocomplete = ({ uniqId, config }) => {

	useEffect(() => {
		new window.Autocomplete(document.getElementById(uniqId), config);
	}, []);

	return (
		<div id={uniqId} />
	)
};

Autocomplete.propTypes = {
	uniqId: PropTypes.string.isRequired,
	config: PropTypes.object,
};

export default Autocomplete;