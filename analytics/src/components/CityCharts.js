import React from 'react';
import PropTypes from 'prop-types';
import ReactHighcharts from 'react-highcharts';

const CityCharts = ({ city, cityData }) => (
	<div className="block">
		{
			cityData ?
				<div>
					<ReactHighcharts
						config={{
							chart: { height: 200 },
							title: {
								text: `${city.city} Population`
							},
							xAxis: {
								categories: cityData.map((c) => c.year)
							},
							yAxis: { title: false },

							series: [{
								name: 'Population',
								data: cityData.map((c) => c.population)
							}]
						}}
					/>
					<br/>
					<ReactHighcharts
						config={{
							chart: { height: 200 },
							title: {
								text: `${city.city} Cost Of Living`
							},
							xAxis: {
								categories: cityData.map((c) => c.year)
							},
							yAxis: { title: false },
							plotOptions: { line: { color: 'green' } },
							series: [{
								name: 'Cost Of Living',
								data: cityData.map((c) => c["cost-of-living"])
							}]
						}}
					/>
					<br/>
				</div> :
				<h3>Choose city first</h3>
		}
	</div>
);

CityCharts.propTypes = {
	city: PropTypes.object,
	cityData: PropTypes.arrayOf(PropTypes.object),
};

export default CityCharts;
