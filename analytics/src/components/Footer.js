import React from 'react';
import '../styles/Footer.less';

const Footer = () => (
	<div className="footer">
		All Rights Reserved
	</div>
);

export default Footer;
