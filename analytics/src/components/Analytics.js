import React, { useState, useEffect } from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import CityAutocomplete from './CityAutocomplete';
import CityCharts from './CityCharts';
import '../styles/Analytics.less'

const Analytics = () => {
	const [currentCity, setCurrentCity] = useState(null);
	const [cityData, setCityData] = useState(null);

	useEffect(() => {
		if (!currentCity) return;

		const { data_file } = currentCity;

		(async () => {
			const response = await fetch(`fixtures/data/${data_file}`);
			const cityData = await response.json();

			setCityData(cityData);
		})()

	}, [currentCity]);

	return (
		<Container className="cities-analytics" fluid>
			<Row>
				<Col xs={12} md={12} lg={6}>
					<CityAutocomplete onResultSelected={setCurrentCity} />
				</Col>
				<Col xs={12} md={12} lg={6}>
					<CityCharts cityData={cityData} city={currentCity} />
				</Col>
			</Row>
		</Container>
	);
};

export default Analytics;
