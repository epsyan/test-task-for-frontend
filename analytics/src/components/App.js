import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Analytics from './Analytics';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/main.less';

const App = () => {
	return (
		<>
			<Header />
			<Analytics />
			<Footer />
		</>
	);
};

export default App;