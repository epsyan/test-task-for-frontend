import React from 'react';
import '../styles/Header.less'

const Header = () => (
	<div className="header">
		<h1>Clearmove City Analytics</h1>
	</div>
);

export default Header;
