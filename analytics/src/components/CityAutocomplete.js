import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Autocomplete from './Autocomplete';
import '../styles/CityAutocomplete.less';

const CityAutocomplete = ({ onResultSelected }) => {
	const [cities, setCities] = useState(null);

	useEffect(() => {
		(async () => {
			const response = await fetch('fixtures/cities.json');
			const cities = await response.json();

			setCities(cities);
		})();

	}, []);

	return (
		<div className="block city-autocomplete">
			{
				cities ?
					<>
						<h3>Choose city:</h3>
						<Autocomplete
							uniqId="cityAutocomplete"
							config={
								{
									placeholder: 'Type to search...',
									data: [
										{
											source: () => cities,
											sourceSearchField: 'city'
										}
									],
									onResultSelected: (city) => {
										onResultSelected(cities.find(c => c.city === city))
									}
								}
							}
						/>
					</> :
					<div>Loading...</div>
			}
		</div>
	);
};

CityAutocomplete.propTypes = {
	onResultSelected: PropTypes.func.isRequired,
};

export default CityAutocomplete;
